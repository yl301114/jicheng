<div class="contact <?php echo $contactClass ?>">
    <div class="contact_content">
        <h3 class="second-title text-center mb-10">聯絡我們</h3>
        <p class="text-gray text-center">如欲進一步諮詢報價或由專人協助，請留下聯絡資訊，我們將主動與您聯絡。</p>
        <form action="">
            <div class="row">
                <div class="col-md-6">
                    <div class="input-box">
                        <label for="">您的姓名</label>
                        <input type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-box">
                        <label for="">代表單位</label>
                        <input type="text">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-box">
                        <label for="">電子信箱</label>
                        <input type="email">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="input-box">
                        <label for="">聯絡電話</label>
                        <input type="phone">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="input-box">
                        <label for="">標題</label>
                        <input type="text">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="input-box">
                        <label for="">內容</label>
                        <textarea name="" id="" cols="30" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <input class="submit" type="submit" value="送出">
        </form>
    </div>
</div>