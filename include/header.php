    <header>
        <div class="container d-flex align-items-center justify-content-between">
            <div class="left">
                <a class="d-flex align-items-center justify-content-center" href="./index.php"><img src="./public/img/logo.png" alt=""></a>
            </div>
            <div class="right d-flex align-items-center">
                <nav class="navigation">
                    <ul class="d-lg-flex d-block align-items-center flex-wrap h-100">
                        <li class="stage_first drop <?php if($pageName == "introduction") echo "active" ?>">
                            <a href="./product-introduction.php">冷鏈技術</a>
                            <ul>
                                <li class="stage_second"><a href="./product-introduction.php">產品介紹</a></li>
                                <li class="stage_second"><a href="./product-buy.php">買賣</a></li>
                                <li class="stage_second"><a href="./product-rent.php">租賃</a></li>
                                <li class="stage_second"><a href="./consult.php">技術諮詢</a></li>
                            </ul>
                        </li>
                        <li class="stage_first <?php if($pageName == "business") echo "active" ?>"><a href="./business.php">公司業務</a></li>
                        <li class="stage_first <?php if($pageName == "news") echo "active" ?>"><a href="./news.php">即時資訊</a></li>
                        <li class="stage_first <?php if($pageName == "education") echo "active" ?>"><a href="./education.php">教育專區</a></li>
                        <li class="stage_first drop <?php if($pageName == "about") echo "active" ?>">
                            <a href="./about.php">關於我們</a>
                            <ul>
                                <li class="stage_second"><a href="./about.php">企業簡介</a></li>
                                <li class="stage_second"><a href="./team.php">團隊組成</a></li>
                            </ul>
                        </li>
                        <li class="stage_first"><a href="">聯絡我們</a></li>
                        <li class="stage_first <?php if($pageName == "laboratory") echo "active" ?>"><a href="./laboratory.php">實驗室</a></li>
                    </ul>
                </nav>
                <div class="icon-menu d-lg-none d-block">
                    <div class="bar top"></div>
                    <div class="bar middle"></div>
                    <div class="bar bottom"></div>
                </div>
                <!-- <div class="lang d-flex align-items-center">
                    <img src="./public/img/icon_globe.png" alt="">
                    <a class="active" href="#">CH</a>
                    <a href="#">EN</a>
                </div> -->
            </div>
        </div>
    </header>