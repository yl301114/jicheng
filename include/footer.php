    <footer>
        <div class="container d-flex align-items-end justify-content-between flex-wrap">
            <div class="left d-lg-flex d-block align-items-center flex-wrap">
                <img class="logo" src="./public/img/logo-white.png" alt="">
                <ul>
                    <li class="company">吉晟藥品股份有限公司</li>
                    <li>
                        <img src="./public/img/footer_address.svg" alt="">
                        <span>台北市內湖區舊宗路二段171巷17號1樓</span>
                    </li>
                    <li>
                        <img src="./public/img/footer_phone.png" alt="">
                        <span>+886-2-2797-0177</span>
                    </li>
                    <li>
                        <img src="./public/img/footer_fax.png" alt="">
                        <span>+886-2-2797-0155</span>
                    </li>
                    <li>
                        <img src="./public/img/footer_email.png" alt="">
                        <span>info@veritas-biomedical.com</span>
                    </li>
                </ul>
            </div>
            <div class="right">
                <p>Copyright © 2021 VERITAS BIOMEDICAL COMPANY All rights reserved</p>
            </div>
        </div>
    </footer>
    <div class="gototop d-flex flex-wrap align-item-center">
        <img src="./public/img/arrow-top.svg" alt="">
        <p>TOP</p>
    </div>
    <script src="./public/js/jquery/jquery.min.js"></script>
    <script src="./public/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="./public/js/swiper/swiper-bundle.min.js"></script>
    <script src="./public/js/index.js"></script>