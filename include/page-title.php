<div class="page-title">
    <img src="<?php echo $pagePic ?>" alt="">
    <div class="title-mask"></div>
    <div class="text">
        <h1><?php echo $pageTitle[0] ?></h1>
        <h2><?php echo $pageTitle[1] ?></h2>
    </div>
</div>