<?php
    $pageTitle = ["產品介紹","INTRODUCTION"];
    $pagePic = '/public/img/page_title_product.png';
    $pageName = "introduction";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <h3 class="second-title text-center mt-30 mb-30">產品列表</h3>
        <div class="tab d-flex align-items-center justify-content-center mb-45">
            <a class="active" href="./product-buy.php">購買</a>
            <a href="./product-rent.php">租賃</a>
        </div>
        <div class="list-pic-2">
            <div class="container mb-40">
                <div class="row">
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-1.jpg" alt=''></div>
                        <h4>Crēdo Cube™重複保溫箱（小）</h4>
                        <p>容量2、4、8、12、16與28 L；三種溫層-18℃、2~8℃與15~25 ℃；保溫時數依容量與溫層而有所不同，最長可至96小時。（2~8L另有拉鍊外袋可選）</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-2.png" alt=''></div>
                        <h4>Crēdo Cube™重複保溫箱（大）</h4>
                        <p>容量56與96 L；兩種溫層2~8℃與15~25℃；保溫時數為96小時。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-3.jpg" alt=''></div>
                        <h4>Crēdo ProMed™重複保溫箱</h4>
                        <p>容量2、4與8 L；兩種溫層2~8℃與15~25℃；保溫時數依容量而有所不同，最長可至72小時。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-4.jpg" alt=''></div>
                        <h4>Crēdo Xtreme™重複保溫櫃</h4>
                        <p>棧板為ISO規格，半板371 L，全板807 L；種溫層-18℃、2~8℃或15~25℃；保溫時數依溫層而有所不同，最長可至120小時。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-5.png" alt=''></div>
                        <h4>CoolGuard™ PCM單次保溫箱</h4>
                        <p>容量6、12、28、56與96 L；兩種溫層2~8℃與15~25℃；保溫時數最長可至72小時。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-6.png" alt=''></div>
                        <h4>CoolGuard™ Advance單次保溫箱</h4>
                        <p>容量4、12、28、56與96 L；三種溫層-18℃、2~8℃與15~25℃；保溫時數最長可至120小時。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-buy-7.jpg" alt=''></div>
                        <h4>NanoCool™ Universal單次保溫盒</h4>
                        <p>按壓後冷卻至2~8 ℃，可立即使用的單次溫控包材。容量約1.8與6 L；保溫時數有48與96小時可選擇。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                </div>
            </div>
            <!-- <ul class="pagination d-flex align-items-center justify-content-center mb-100">
                <li class="arrow prev"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="arrow next"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
            </ul> -->
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>