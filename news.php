<?php
    $pageTitle = ["即時資訊","LATEST NEWS "];
    $pagePic = '/public/img/page_title_news.png';
    $pageName = "news";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="container">
            <div class="d-lg-flex d-block align-items-center justify-content-between mb-70">
                <div class="search mx-lg-0 mx-auto mb-lg-0 mb-4">
                    <div class="icon"><img src="./public/img/search.svg" alt=""></div>
                    <input type="text" placeholder="搜尋最新消息">
                </div>
                <div class="tab-box d-lg-block d-flex justify-content-center">
                    <button class="active">依照時間排序</button>
                    <button>依照瀏覽數排序</button>
                </div>
            </div>
            <div class="list-text">
                <?php for($x=1; $x<=6; $x++) {
                    echo "<div class='inner'>
                            <a href='./news-content.php' class='list-text_title news'>
                                <div class='tag-box'><div class='tag'>標籤</div></div>
                                <div class='news-content'>
                                    <h3>最新消息標題最新消息標題ㄧ</h3>
                                    <span class='date'>2021.08.15</span>
                                </div>
                            </a>
                        </div>";
                    }
                ?>
            </div>
            <ul class="pagination d-flex align-items-center justify-content-center mb-100">
                <li class="arrow prev"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="arrow next"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
            </ul>
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>