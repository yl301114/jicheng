<?php
    $pageTitle = ["關於我們","ABOUT US"];
    $pagePic = '/public/img/page_title_about.png';
    $pageName = "about";
    $contactClass = "gray-bg"
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="list-pic-1">
            <div class="inner">
                <div class="container d-flex align-items-center flex-wrap">
                    <img src="./public/img/about-1.png" alt="">
                    <div class="text">
                        <h3 class="second-title">企業簡介</h3>
                        <p>
                            吉晟藥品股份有限公司成立於2021年5月，本公司董監事均為國內知名藥品公司之創辦人。吉晟藥品擁有先進的生技醫藥業專用之冷鏈技術與材料，亦將上市AI人工智慧協助醫師快速診斷中風；同時，設有全國最專精於神經免疫科學實驗室，以及先進的生醫材料開發部門。<br>
                            <br>
                            吉晟藥品將持續各項醫療技術之開發，投資員工、創造價值、提升競爭力。秉持公司五大核心價值，擴展業務範疇，將研發成果回饋股東、分享員工、服務客戶並嘉惠病患。
                        </p>
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="container d-flex align-items-center flex-wrap">
                    <img src="./public/img/about-2.png" alt="">
                    <div class="text">
                        <h3 class="second-title">宗旨理念</h3>
                        <p>
                        以提供專業技術為基礎的各項科技產品，服務醫師及照護病患。我們的五大核心價值就是專業、科技、熱忱、責任與關懷。
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container develop">
            <h3 class="second-title text-center mb-20">發展歷程</h3>
            <img src="./public/img/about-3.png" alt="">
        </div>
        <?php include("include/contact.php") ?>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>