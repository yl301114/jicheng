<?php
    $pageTitle = ["實驗室","LABORATORY"];
    $pagePic = '/public/img/page_title_laboratory.png';
    $pageName = "laboratory";
    $contactClass = "gray-bg";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="container">
            <div class="tab-box justify-content-center tab-link mb-70">
                <button><a href="./laboratory-news.php">即時資訊</a></button>
                <button><a href="./detect.php">檢測項目介紹</a></button>
                <button class="active"><a href="./download.php">表單下載</a></button>
            </div>
            <div class="download-intro">
                <div class="download row">
                    <h3 class="second-title mb-20">表單下載</h3>
                    <div class="col-lg-4 col-md-6">
                        <div class="download_content">
                            <span>表單名稱表單名稱表單名稱</span>
                            <img src="./public/img/icon_download.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="download_content">
                            <span>表單名稱表單名稱表單名稱</span>
                            <img src="./public/img/icon_download.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="download_content">
                            <span>表單名稱表單名稱表單名稱</span>
                            <img src="./public/img/icon_download.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="download_content">
                            <span>表單名稱表單名稱表單名稱</span>
                            <img src="./public/img/icon_download.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="download_content">
                            <span>表單名稱表單名稱表單名稱</span>
                            <img src="./public/img/icon_download.png" alt="">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="download_content">
                            <span>表單名稱表單名稱表單名稱</span>
                            <img src="./public/img/icon_download.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include("include/contact.php") ?>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>