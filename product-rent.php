<?php
    $pageTitle = ["產品介紹","INTRODUCTION"];
    $pagePic = '/public/img/page_title_product.png';
    $pageName = "introduction";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <h3 class="second-title text-center mt-30 mb-30">產品列表</h3>
        <div class="tab d-flex align-items-center justify-content-center mb-45">
            <a href="./product-buy.php">買賣</a>
            <a class="active" href="./product-rent.php">租賃</a>
        </div>
        <div class="list-pic-2">
            <div class="container mb-40">
                <div class="row">
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-rent-1.jpg" alt=''></div>
                        <h4>Crēdo Cube™重複保溫箱</h4>
                        <p>依客戶所需的裝存容量、運送溫度條件、保溫時數與使用天數，提供合適的方案。</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                    <div class="col-lg-4 col-6">
                        <div class="pic"><img src="./public/img/product-rent-2.jpg" alt=''></div>
                        <h4>Crēdo Xtreme™重複保溫櫃</h4>
                        <p>依客戶所需的裝存容量、運送溫度條件、保溫時數與使用天數，提供合適的方案。（可協助與原廠接洽Crēdo™ Cargo租賃）</p>
                        <a class='readmore' href='./product-spec.php'>查看更多</a>
                    </div>
                </div>
            </div>
            <!-- <ul class="pagination d-flex align-items-center justify-content-center mb-100">
                <li class="arrow prev"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li class="arrow next"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
            </ul> -->
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>