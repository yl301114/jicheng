<?php
    $pageTitle = ["關於我們","TEAM MEMBERS"];
    $pagePic = '/public/img/page_title_about.png';
    $pageName = "about";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <h3 class="second-title mb-40 text-center">團隊組成</h3>
        <div class="team">
            <div class="inner">
                <div class="pic">
                    <div class="pic-inner">
                        <img src="/public/img/member-1.png" alt="">
                    </div>
                </div>
                <div class="text">
                    <div class="member-name">
                        <h4><span class="chinese">楊朝堂</span><span class="english">John Yang</span></h4>
                        <p>董事長</p>
                    </div>
                    <div class="text_content">
                        <div class="text_content_title">學歷</div>
                        <ul>
                            <li>MBA, Asian Institute of Management, Philippines</li>
                        </ul>
                    </div>
                    <div class="text_content history">
                        <div class="text_content_title">經歷</div>
                        <ul>
                            <li><span>National Sales Manager, Pfizer Taiwan</span><span>1989~1993</span></li>
                            <li><span>G.M., Weurth Company Taiwan</span><span>1994~2001</span></li>
                            <li><span>G.M., Apex Pharma Taiwan</span><span>2001~2007</span></li>
                            <li><span>President of Uni Pharma</span><span>2008~2015</span></li>
                            <li><span>Board of Uni Pharma</span><span>2001~current</span></li>
                            <li><span>Board of Giddi Pharma Taiwan</span><span>1998~current</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="pic">
                    <div class="pic-inner">
                        <img src="/public/img/member-2.png" alt="">
                    </div>
                </div>
                <div class="text">
                    <div class="member-name">
                        <h4><span class="chinese">林亮光</span><span class="english">Terry Lin R.Ph, LLM</span></h4>
                        <p>總經理</p>
                    </div>
                    <div class="text_content">
                        <div class="text_content_title">學歷</div>
                        <ul>
                            <li>College of Pharmacy, Taipei Medical University</li>
                            <li>LLM, National Chengchi University</li>
                        </ul>
                    </div>
                    <div class="text_content history">
                        <div class="text_content_title">經歷</div>
                        <ul>
                            <li><span>District Manager, Rhone Poulenc Rorer</span><span>1998~2000</span></li>
                            <li><span>Marketing & Sales Director, Uni Pharma</span><span>2000~2001</span></li>
                            <li><span>G.M., Uni Pharma</span><span>2002~2021</span></li>
                            <li><span>Board of Uni Pharma</span><span>2002~2021</span></li>
                            <li><span>Supervisor of Genovate-Biotechnology</span><span>2014~2017</span></li>
                            <li><span>G.M., Veritas Biomedical</span><span>2021~current</span></li>
                            <li><span>Board of Veritas Biomedical</span><span>2021~current</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="pic">
                    <div class="pic-inner">
                        <img src="/public/img/member-3.png" alt="">
                    </div>
                </div>
                <div class="text">
                    <div class="member-name">
                        <h4><span class="chinese">陳慧玲</span><span class="english">Grace Chen</span></h4>
                        <p>財務長</p>
                    </div>
                    <div class="text_content">
                        <div class="text_content_title">學歷</div>
                        <ul>
                            <li>EMBA, National Chengchi University</li>
                        </ul>
                    </div>
                    <div class="text_content history">
                        <div class="text_content_title">經歷</div>
                        <ul>
                            <li><span>Manager of TECO image system and Creative Sensor Inc.</span><span>1995~2004</span></li>
                            <li><span>Director of Answer Technology Co., Ltd.</span><span>2004~2005</span></li>
                            <li><span>Director of Multilite international Co., Ltd.</span><span>2005~2010</span></li>
                            <li><span>Director of TECO image system Co., Ltd.</span><span>2011~2013</span></li>
                            <li><span>Director of Pharmigene Inc.</span><span>2013~2014</span></li>
                            <li><span>CFO, Uni Pharma</span><span>2015~2021</span></li>
                            <li><span>CFO, Veritas Biomedical</span><span>2021~current</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="pic">
                    <div class="pic-inner">
                        <img src="/public/img/member-4.jpg" alt="">
                    </div>
                </div>
                <div class="text">
                    <div class="member-name">
                        <h4><span class="chinese">黃俊達</span><span class="english">Marc Huang</span></h4>
                        <p>醫學部部長</p>
                    </div>
                    <div class="text_content">
                        <div class="text_content_title">學歷</div>
                        <ul>
                            <li>Bicol Christian College of Medicine, Doctor of Medicine, Philippines</li>
                        </ul>
                    </div>
                    <div class="text_content history">
                        <div class="text_content_title">經歷</div>
                        <ul>
                            <li><span>Intern & Resident, Cathay General hospital</span><span>1998~2012</span></li>
                            <li><span>Resident, Cardinal Tien Hospital</span><span>2013~2014</span></li>
                            <li><span>Medical Manager, Uni Pharma</span><span>2014~2021</span></li>
                            <li><span>ISO 17025 Lab. Director, Uni Pharma</span><span>2016~2021</span></li>
                            <li><span>Medical Head & Lab. Director, Veritas Biomedical</span><span>2021~currentt</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>