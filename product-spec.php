<?php
    $pageTitle = ["產品介紹","INTRODUCTION"];
    $pagePic = '/public/img/page_title_product.png';
    $pageName = "introduction";
    $contactClass = "white-bg";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="container">
            <div class="row align-items-center product-content">
                <div class="col-md-6">
                    <img src='./public/img/product-3.png' alt=''>
                </div>
                <div class="col-md-6">
                    <div class="text">
                        <h2>產品名稱產品名稱產品名稱產品名稱</h2>
                        <div class="classify">
                            <span>冷媒類型名稱</span><span>產品類型名稱</span>
                        </div>
                        <p>
                            文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述<br><br>
                            文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述
                        </p>
                    </div>
                </div>
            </div>
            <div class="row product-spec">
                <div class="col-md-6">
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/Temperature.png" alt=""></div>
                            <span>溫層</span>
                        </div>
                        <div class="select-box">
                            <select name="" id="">
                                <option value="選項一">選項一</option>
                            </select>
                        </div>
                        <img class="question" src="./public/img/icon_q.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/Packing.png" alt=""></div>
                            <span>容量</span>
                        </div>
                        <div class="select-box">
                            <select name="" id="">
                                <option value="選項一">選項一</option>
                            </select>
                        </div>
                        <img class="question" src="./public/img/icon_q.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/clock-checked.png" alt=""></div>
                            <span>保溫時長</span>
                        </div>
                        <div class="select-box">
                            <select name="" id="">
                                <option value="選項一">選項一</option>
                            </select>
                        </div>
                        <img class="question" src="./public/img/icon_q.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/day-off.png" alt=""></div>
                            <span>租賃天數</span>
                        </div>
                        <div class="product-num d-flex align-items-center">
                            <div class="inner">
                                <div class="product-num_btn minus"><img src="./public/img/minus.svg"></div>
                                <input type="number" value="0">
                                <div class="product-num_btn plus"><img src="./public/img/plus.svg"></div>
                            </div>
                        </div>
                        <img class="question" src="./public/img/icon_q.png" alt="">
                    </div>
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/Boxes.png" alt=""></div>
                            <span>數量</span>
                        </div>
                        <div class="product-num d-flex align-items-center">
                            <div class="inner">
                                <div class="product-num_btn minus"><img src="./public/img/minus.svg"></div>
                                <input type="number" value="0">
                                <div class="product-num_btn plus"><img src="./public/img/plus.svg"></div>
                            </div>
                        </div>
                        <img class="question" src="./public/img/icon_q.png" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/Torrent.png" alt=""></div>
                            <span>規格尺寸</span>
                        </div>
                        <div class="spec-content">200x200x50 mm</div>
                    </div>
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/bed-size.png" alt=""></div>
                            <span>包材尺寸</span>
                        </div>
                        <div class="spec-content">310x310x306 mm</div>
                    </div>
                    <div class="d-flex align-items-center flex-wrap mb-15">
                        <div class="item-name">
                            <div class="icon"><img src="./public/img/weight-kg.png" alt=""></div>
                            <span>包材重量</span>
                        </div>
                        <div class="spec-content">4.9 kg</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-gray py-60 rent-intro">
            <div class="container px-20">
                <h3 class="second-title text-center mb-20">租賃說明</h3>
                <p class="text-gray mb-20 opacity-8">
                    文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字<br><br>
                    文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字<br><br>
                    文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字
                </p>
                <hr>
            </div>
            <div class="container px-20">
                <p class="text-gray m-0">*若有零件單買需求，請另洽公司業務人員。</p>
            </div>
        </div>
        <?php include("include/contact.php") ?>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>