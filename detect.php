<?php
    $pageTitle = ["實驗室","LABORATORY"];
    $pagePic = '/public/img/page_title_laboratory.png';
    $pageName = "laboratory";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="container">
            <div class="tab-box justify-content-center tab-link mb-70">
                <button><a href="./laboratory-news.php">即時資訊</a></button>
                <button class="active"><a href="./detect.php">檢測項目介紹</a></button>
                <button><a href="./download.php">表單下載</a></button>
            </div>
            <div class="detect-intro">
                <h3 class="second-title mb-20">檢測項目介紹</h3>
                <div class="search mb-45">
                    <div class="icon"><img src="./public/img/search.svg" alt=""></div>
                    <input type="text" placeholder="搜尋最新消息">
                </div>
                <div class="list-text detect">
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Neuromyelitis Optica Spectrum Disorder(NMOSD)／泛視神經脊髓炎</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共2項，AQP4和MOG（均可單項檢測與抗體titer測定）</li>
                                <li>NMOSD是一種中樞神經系統自體免疫疾病，主要造成視神經和脊髓反覆發炎，也會攻擊腦和腦幹。NMOSD的臨床診斷標準中，需參考腦與脊髓MRI影像，以及水通道蛋白4 (Aquaporin 4, AQP4)自體抗體。少部份NMOSD病患的血清中沒有anti-AQP4抗體反應，其中少數病患則是找到anti-MOG(myelin oligodendrocyte glycoprotein)抗體反應。</li>
                                <li>檢體：血清</li>
                                <li>方法學：EUROIMMUN IIFT, cell-based</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Myasthenia Gravis(MG) Panel／重症肌無力套組</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共3項，MuSK, Ɛ-AChR與γ-AChR（MuSK可單項檢測）</li>
                                <li>MG為身體免疫系統異常的疾病，體內產生不正常的抗體侵犯神經肌肉系統，造成肌肉無力的現象。發病年齡成雙峰分佈，20~30歲間以女性為主，而50~60歲間則以男性居多。臨床表現為肌肉無力和容易疲憊，以眼皮下垂或複視困擾的眼外肌無力最為常見，若合併肢體、頸部、臉部，吞嚥甚至呼吸肌的無力，稱為全身性的肌無力症。病因主要是體內產生抗乙醯膽鹼抗體，侵犯神經肌肉接合處的乙醯膽鹼受器。因此，測試抗體濃度為鑑別此疾病的方法之一。</li>
                                <li>檢體：血清</li>
                                <li>方法學：EUROIMMUN, IIFT cell-based（3項抗體合併檢測）；IBL, ELISA（MuSK單項檢測）</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Limbic encephalitis(LE)／邊緣性腦炎</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共6項，NMDAR、AMPAR1/2、CASPR2、LGI1、GABAβR和DPPX。</li>
                                <li>自體免疫腦炎臨床特徵包括多種精神與神經症狀，主要分為兩大類：第一類是產生抗體對抗細胞內抗原，與癌有相關；第二類是出現抗體對抗神經元表面抗原，好發於海馬迴和顳葉等邊緣系統。最廣為人知就屬anti-NMDAR腦炎，因為體內腫瘤產生抗體結合於海馬迴上的NMDA受體，以致破壞受體細胞，使腦部記憶認知功能損害，病患初期會有類似感冒的症狀，進而出現癲癇、精神異常、神經認知缺損以及自律神經失調等症狀。</li>
                                <li>檢體：血清、腦脊髓液</li>
                                <li>方法學：EUROIMMUN, IIFT cell-based</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Paraneoplastic Neurologic Syndrome(PNS)／腫瘤伴生神經症候群</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共12項，Amphiphsyin、CV2、PNMA2(Ma2/Ta)、Ri、Yo、Hu、Recoverin、SOX1、Titin、Zic4、GAD65和Tr(DNER)</li>
                                <li>排除腫瘤轉移、腫瘤有關的感染、凝血、代謝或營養疾病及癌症治療副作用等因素後，若神經系統外的腫瘤對肌肉、周邊神經、脊髓、腦幹、小腦及大腦半球所產生多種神經疾病症狀，可稱為PNS。免疫系統將腫瘤的部分抗原，辨認為外來抗原，產生抗體反應，由於神經系統的抗原與腫瘤產生的抗原過於相似，使這些抗體攻擊神經系統的抗原，引起神經學症狀。</li>
                                <li>檢體：血清</li>
                                <li>方法學：EUROIMMUN, EUROBlotOne</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Polyneuropathies／多發性神經病變</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：分IgG與IgM兩類。每類共7項，GM1、GM2、GM3、GD1a、GD1b、GT1b和GQ1b另有anti-MAG單項檢測</li>
                                <li>Polyneuropathies是周邊神經系統常見的疾病，特徵為四肢末梢感覺異常及麻木、疼痛，有時合併肌肉萎縮、無力，所以神經病變在四肢末梢較明顯。造成Polyneuropathies的原因相當多，糖尿病是常見的病因之一，然而，急性發炎性去髓鞘多發性神經病變則是一種發生快速且可能急速惡化的疾病。其致病機轉，可能是來自於免疫事件所產生的反應，因為外來的致病原，如病毒、細菌，或是被視為外來物的腫瘤及胎兒，所會引發身體的免疫反應，進而產生抗體，這些抗體反而攻擊了自身神經系統，進而產生神經病變。</li>
                                <li>檢體：血清</li>
                                <li>方法學：EUROIMMUN, EUROBlotOne（7項抗體檢測）；EUROIMMUN, IIFT tissue-based（anti-MAG）</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Autoimmune Inflammatory Myopathies(AIM)／自體免疫肌肉發炎性病變</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共18項，Mi2α、Mi2β、TIF1γ、MDA5、NXP2、SAE1、Ku、PM-Scl100、PM-Scl75、Jo-1、SRP、PL-7、PL-12、EJ、OJ、Ro-52、cN-1A和HMGCR。</li>
                                <li>AIM是一種侵犯骨骼肌肉為主的自體免疫性疾病，其中最常見的是皮肌炎(Dermatomyositis, DM)和多發性肌炎(Polymyositis, PM)。因為肌肉組織產生發炎的狀況，進而導致肌肉纖維損傷，臨床表現主要是漸進性的肌肉無力。目前AIM診斷的國際共識，在免疫學檢查的部份，是檢測血清中特殊肌炎抗體。</li>
                                <li>檢體：血清</li>
                                <li>方法學：EUROIMMUN, EUROBlotOne</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Celiac disease／乳糜瀉</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共2項，tTG和GAF-3X</li>
                                <li>乳糜瀉屬於遺傳性的自體免疫腸胃疾病，由於飲食中攝入麩質（一種存在於小麥、黑麥和大麥中的蛋白質）產生免疫反應的不耐症，長期導致小腸絨毛與內膜受損，進而使身體無法正常吸收營養物質，造成許多症狀產生，例如貧血、模糊的腹部症狀、食道炎、神經病變、共濟失調、憂鬱症、身材矮小、軟骨症以及許多其他症狀。全球約有1%的人深受此疾病影響。可透過測定抗組織麩醯胺酸轉移酶(tTG)和麥膠蛋白(GAF-3X)抗體來做鑑別。</li>
                                <li>檢體：血清</li>
                                <li>方法學：EUROIMMUN, EUROBlotOne</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Alzheimer’s Disease(AD) Panel／阿茲海默症套組</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共5項，Aβ40、Aβ42、Total Tau、p-Tau 181和TDP43</li>
                                <li>阿茲海默症是一種不可逆，進展性的腦部疾病，會導致記憶、思考和行為問題。疾病特徵為乙型類澱粉蛋白(β-Amyloid, Aβ)堆積成斑塊和神經纖維糾結、腦神經細胞間連結喪失、以及神經細胞的凋亡。Aβ40是斑塊中的主要乙型類澱粉蛋白成分，近期研究發現，Aβ42/Aβ40比值數據與AD的病程相關。Tau蛋白是連結微管骨架的成分，主要功能是維持神經元的形狀，當Tau蛋白磷酸化逐漸失控，會從細胞骨架上脫落，在神經元裡聚集形成糾結。TDP432蛋白與Aβ反應形成寡聚體，造成神經突觸功能障礙，增加腦部發炎反應。測試血中這幾項重要蛋白標的有助瞭解AD病程。</li>
                                <li>檢體：血清、血漿</li>
                                <li>方法學：Quanterix Simoa HD-X, Digital ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Parkinson’s disease(PD)／巴金森氏症</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：α-Synuclein</li>
                                <li>PD屬於慢性神經退化性疾病，特徵有顫抖、肢體僵硬、動作遲緩、姿態不穩等症狀。α-突觸核蛋白(α-Synuclein)是一種與PD發生密切相關的蛋白質，α-Synuclein集中在神經元的突觸前神經末梢和神經元的細胞核中。當若α-synuclein基因突變，導致有缺陷的α-Synuclein蛋白異常堆積形成聚合產物，使腦神經元受到損傷。α-synuclein在PD的病理機轉中，佔相當重要的位置。</li>
                                <li>檢體：血清</li>
                                <li>方法學：Quanterix Simoa HD-X, Digital ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Neurofilament Light chain(NfL)／神經元絲輕鏈</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>神經元細胞骨架由神經元絲(Neurofilament)、微管、微絲，這三種纖維構成。神經元絲依分子量大小可再區分NfH、NfM與NfL，其中NfL在神經元中表達68 kDa細胞骨架中間絲蛋白。神經元絲除構成神經元細胞骨架外，也參與神經元內的代謝產物和離子運輸流動的通路。當腦神經元損傷、退化或疾病，神經絲可以在軸突損傷或神經元變性後大量釋放。許多研究指出，NfL與創傷性腦損傷、多發性硬化症、額顳葉癡呆和其他神經退行性疾​​病有關。</li>
                                <li>檢體：血清</li>
                                <li>方法學：Quanterix Simoa HD-X, Digital ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Glial Fibrillary Acidic Protein(GFAP)／膠質纖維酸性蛋白</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>GFAP屬於第III型中間絲蛋白，主要在中樞神經系統的星形膠質細胞中表達，可作為星形膠質細胞的特異性生物標記物。GFAP對星形膠質細胞的支持、引導、發育神經元結構和活動訊號方面發揮關鍵作用，像是細胞通訊和血腦屏障的功能。若發生中樞神經系統損傷，人體內的GFAP表現就會增加，許多研究發現GFAP和多項疾病相關，如創傷性腦損傷、中風、腦腫瘤、退化性神經病變等。</li>
                                <li>檢體：血清</li>
                                <li>方法學：Quanterix Simoa HD-X, Digital ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Inflammation Panel／發炎因子套組</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>檢測標的：共10項，GM-CSF、IFN-γ、IL-1β、IL-2, IL-4, IL-5, IL-6, IL-8, IL-10和TNF-α</li>
                                <li>人體在正常情況下若遇到受傷或感冒時，體內的免疫系統會啟動發炎反應，抵禦外來病原體。因人體有個體間的差異，若過度或不適當地啟動免疫系統，造成免疫細胞釋放過多的發炎因子進入血液之中，反而會導致細胞受損、器官功能受到嚴重損害。
                                    <ul class="ps-4 pt-0">
                                        <li>顆粒球巨噬細胞株刺激因子(GM-CSF)讓幹細胞與白血球先驅細胞分化或分裂，產生粒細胞和單核球，與巨噬細胞和發炎症狀有關，是促進發炎因子的一部分。</li>
                                        <li>干擾素Gamma(IFN-γ)是II型干擾素，是活化巨噬細胞的因子。在發炎的條件下，IFN-γ由活化T細胞和NK細胞產生，具有抗病毒、免疫調節及抗腫瘤特性。</li>
                                        <li>介白素(Interleukins, IL)主要作用在免疫細胞分化與激發，可促進發炎或消炎。IL-1β是促進發炎的細胞因子，可通過直接和間接機制介導宿主對感染的反應。IL-2是調節淋巴球免疫活性的細胞因子。IL-4是Th2細胞分泌的細胞因子，在調節體液免疫和適應性免疫中起關鍵作用。IL-5是讓嗜酸性白血球增多的細胞因子。IL-6為促進發炎的細胞因子，在與創傷、外傷、壓力、感染、腦死、腫瘤及其他急性發炎反應病程中均可快速產生，可作為急性發炎的早期指標。IL-8是巨噬細胞和上皮細胞等分泌的細胞因子，對嗜中性白血球有細胞趨化作用，能對發炎反應進行調節。IL-10是抗發炎細胞因子，主要由單核球、Th2細胞和B細胞產生。</li>
                                        <li>腫瘤壞死因子Alpha(TNF-α)由巨噬細胞分泌產生，是促進發炎反應的細胞因子，通過促進腫瘤細胞凋亡、破壞腫瘤組織血管、免疫調節作用、放化療增敏作用等，產生抗腫瘤作用。</li>
                                    </ul>
                                </li>
                                <li>檢體：血清</li>
                                <li>方法學：Quanterix Simoa HD-X, Digital ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Vascular Endothelial Growth Factor(VEGF)／血管內皮生長因子</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>VEGF對血管內皮細胞具有特異性的肝素結合生長因子，可刺激血管生成，誘導內皮細胞增殖，促進細胞遷移，抑制細胞凋亡，增加血管通透性。當VEGF過度表現時，會導致疾病並使癌細胞能夠生長和轉移。</li>
                                <li>檢體：血清</li>
                                <li>方法學：Quanterix Simoa HD-X, Digital ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Neurofascin-155(nf155)／神經束蛋白155</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>nf155是對神經元中的蘭氏結重要的功能蛋白之一。在5-10%的慢性發炎性脫髓鞘多發性神經病變(CIDP)患者中可檢測出nf155的IgG自體免疫抗體。帶此抗體陽性者的特徵是發病年齡較年輕、亞急性且病程進展快速，伴隨有共濟失調、顫抖、末梢無力以及對靜脈注射免疫球蛋白(IVIg) 或類固醇治療的反應不佳，但可能對血漿置換術或利妥昔單株抗體(Rituximab)有反應。</li>
                                <li>檢體：血清</li>
                                <li>方法學：ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>Contactin-1(CNTN1)／接觸蛋白1</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>CNTN1是對神經元中的蘭氏結重要的功能蛋白之一。約5%的慢性發炎性脫髓鞘多發性神經病變(CIDP)患者可檢測到CNTN1的IgG自體免疫抗體。帶此抗體陽性者的特徵是發病年齡較高、亞急性、侵襲性的格林-巴利症候群(Guillain-Barre syndrome, GBS)，伴隨有軸突損傷、運動無力和對靜脈注射免疫球蛋白(IVIg)治療的反應不佳。有一些患者對類固醇或血漿置換治療有反應，部分病例對利妥昔單株抗體(Rituximab)治療有反應。</li>
                                <li>檢體：血清</li>
                                <li>方法學：ELISA</li>
                            </ul>
                        </div>
                    </div>
                    <div class="inner">
                        <div class="list-text_title">
                            <h3>CASPR1／接觸蛋白相關蛋白1</h3>
                        </div>
                        <div class="list-text_content">
                            <ul>
                                <li>CASPR1（也稱paranodin）是對神經元中的蘭氏結重要的功能蛋白之一，可能在軸突和髓鞘形成神經膠質細胞之間的訊號傳導的作用。在先天性髓鞘形成不足的神經病變患者中，發現這種蛋白質的突變，這會導致髓鞘發展異常和神經病變。它也作用在調節AMPA受體與突觸之間的運輸傳導。</li>
                                <li>檢體：血清</li>
                                <li>方法學：ELISA</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <ul class="pagination d-flex align-items-center justify-content-center mb-100">
                    <li class="arrow prev"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li class="arrow next"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                </ul>
            </div>
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>