<?php
    $pageTitle = ["實驗室","LABORATORY"];
    $pagePic = '/public/img/page_title_laboratory.png';
    $pageName = "laboratory";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="container">
            <div class="laboratory-content">
                <div class="text">
                    <h4>分子與細胞生物實驗室 V. Lab.</h4>
                    <p>
                        吉晟藥品股份有限公司秉持社會責任，成立分子與細胞生物實驗室（簡稱V. Lab.），提供神經醫學特殊自體免疫抗體與相關蛋白的檢測，於2021年11月正式營運。採EUROIMMUN與Quanterix™ Simoa™兩大技術平台，導入LIS系統進行各項結果整合，使報告發放與查詢更加迅速且流暢。<br>
                        <br>
	                    實驗室團隊為具有豐富檢測技術與認證經驗的人員組成，且完成ISO 17025或ISO 15189的訓練。人員、環境、設備、檢測方法與驗證等流程均有相應的品質管理準則。
                    </p>
                    <div class="btn-box">
                        <button class="btn-detect"><a href="./detect.php">檢測項目介紹</a></button>
                        <button class="btn-download"><a href="./download.php">表單下載</a></button>
                    </div>
                </div>
                <div class="pic">
                    <img src="/public/img/laboratory-2.JPG" alt="">
                </div>
            </div>
        </div>
        <div class="bg-gray py-100">
            <div class="container">
                <h3 class="second-title mb-20 news-second-title">即時資訊</h3>
                <div class="d-lg-flex d-block align-items-center justify-content-between mb-70">
                    <div class="search mx-lg-0 mx-auto mb-lg-0 mb-4">
                        <div class="icon"><img src="./public/img/search.svg" alt=""></div>
                        <input type="text" placeholder="搜尋最新消息">
                    </div>
                    <div class="tab-box d-lg-block d-flex justify-content-center">
                        <button class="active">依照時間排序</button>
                        <button>依照瀏覽數排序</button>
                    </div>
                </div>
                <div class="list-text">
                    <?php for($x=1; $x<=6; $x++) {
                        echo "<div class='inner'>
                                <a href='./news-content.php' class='list-text_title news'>
                                    <div class='tag-box'><div class='tag'>標籤</div></div>
                                    <div class='news-content'>
                                        <h3>最新消息標題最新消息標題ㄧ</h3>
                                        <span class='date'>2021.08.15</span>
                                    </div>
                                </a>
                            </div>";
                        }
                    ?>
                </div>
                <ul class="pagination d-flex align-items-center justify-content-center">
                    <li class="arrow prev"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li class="arrow next"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                </ul>
            </div> 
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>