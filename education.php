<?php
    $pageTitle = ["教育專區","EDUCATION"];
    $pagePic = '/public/img/page_title_education.png';
    $pageName = "education";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="list-pic-2">
            <div class="container">
                <div class="d-lg-flex d-block align-items-center justify-content-between mb-70">
                    <div class="search mx-lg-0 mx-auto mb-lg-0 mb-4">
                        <div class="icon"><img src="./public/img/search.svg" alt=""></div>
                        <input type="text" placeholder="搜尋最新消息">
                    </div>
                    <div class="tab-box d-lg-block d-flex justify-content-center">
                        <button class="active">依照時間排序</button>
                        <button>依照瀏覽數排序</button>
                    </div>
                </div>
                <div class="row">
                    <?php for($x=1; $x<=9; $x++) {
                        echo "<div class='col-md-4'>
                                <div class='pic'><img src='./public/img/education-1.png' alt=''></div>
                                <h4>文章標題文章標題文章標題一</h4>
                                <p>文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字</p>
                                <a class='readmore' href='./education-content.php'>閱讀更多</a>
                              </div>";
                        } 
                    ?>
                </div>
                <ul class="pagination d-flex align-items-center justify-content-center mb-100">
                    <li class="arrow prev"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li class="arrow next"><a href="#"><img src="./public/img/arrow-pagination.svg" alt=""></a></li>
                </ul>
            </div>
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>