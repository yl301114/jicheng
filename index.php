<?php
    $pageName = "home";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>
</head>
<style>
    main {
        min-height:auto;
    }
    @media screen and (max-width:991px) {
        main {
            min-height:auto;
        }
    }
</style>
<body>
    <?php include("include/header.php") ?>
    <main>
        <div class="swiper-container home-banner">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="./public/img/index01_v1.jpg" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="./public/img/index02_v1.jpg" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="./public/img/index03_v1.jpg" alt="">
                </div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
    </main>
    <?php include("include/footer.php") ?>
    <script>
        var swiper = new Swiper(".home-banner",{
            slidesPerView: 1,
            loop: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            speed:1500,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false,
            },
        })
    </script>
</body>
</html>