// go to top
$(window).on("scroll resize",function(){
    if($(window).scrollTop()>0){
        $(".gototop").addClass("active");
    }else {
        $(".gototop").removeClass("active");
    }
})
$(".gototop").click(function(){
    $("html,body").animate({scrollTop:0},300);
})

// 產品數量
$(".product-num_btn").click(function(){
    let product_num = $(this).parents(".inner").children("input").val();
    if($(this).hasClass("minus")){
        if(product_num > 0) {
            product_num --;
        }else {
            product_num = 0;
        }
    }else if($(this).hasClass("plus")){
        product_num ++;
    }
    $(this).parents(".inner").children("input").val(product_num);
})

// 常見問答
$(".list-text_title").click(function(){
    $(this).toggleClass("active");
    $(this).parents(".inner").children(".list-text_content").slideToggle(300);
})

// menu
$(".icon-menu").click(function(){
    $(".navigation").toggleClass("active");
})