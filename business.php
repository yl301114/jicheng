<?php
    $pageTitle = ["公司業務","BUSINESS"];
    $pagePic = '/public/img/page_title_business.png';
    $pageName = "business";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>
</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
            <div class="list-pic-1">
                <div class="inner">
                    <div class="container d-flex align-items-center flex-wrap">
                        <img src="./public/img/business-1.JPG" alt="">
                        <div class="text">
                            <h3 class="second-title">分子與細胞生物實驗室V. Lab.</h3>
                            <p>
                            V. Lab.提供神經醫學特殊自體免疫抗體與相關蛋白的檢測，運用EUROIMMUN與Quanterix™ Simoa™兩大技術平台，導入LIS進行各項結果整合，使報告發放與查詢更加迅速且流暢。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="inner">
                    <div class="container d-flex align-items-center flex-wrap">
                        <img src="./public/img/business-2.png" alt="">
                        <div class="text">
                            <h3 class="second-title">Peli BioThermal™冷鏈溫控箱、跨國運輸與技術支援</h3>
                            <p>
                            Peli BioThermal™致力於提供符合PIC/s GDP規範的藥品、疫苗、血液製劑或生物性藥品、醫療器材及緊急救護用品等醫藥產品的溫控箱。依照使用者的需求，例如尺寸、運送溫度條件與保冷時數等，本公司可以提供購買與租賃方案，協助規劃符合使用者所需的產品與服務。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="inner">
                    <div class="container d-flex align-items-center flex-wrap">
                        <img src="./public/img/business-3.jpg" alt="">
                        <div class="text">
                            <h3 class="second-title">Nicolab AI影像判讀</h3>
                            <p>
                            Nicolab為2015年分割自阿姆斯特丹醫學中心的新創科技公司，專精於以人工智慧快速診斷梗塞性腦中風。本公司將依循法規程序引進該公司人工智慧產品，盼望能為台灣醫師及病患提供有效的快速診斷方案。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="inner">
                    <div class="container d-flex align-items-center flex-wrap">
                        <img src="./public/img/business-4.png" alt="">
                        <div class="text">
                            <h3 class="second-title">XVIVO Perfusion system器官保存液</h3>
                            <p>
                            XVIVO AB是位於瑞典，致力於開發肺臟移植相關耗材與技術的科技公司。本公司協助國內從事肺臟移植之醫師與醫療機構專案進口肺臟移植保存液(PERFADEX Plus)。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="inner">
                    <div class="container d-flex align-items-center flex-wrap">
                        <img src="./public/img/business-5.PNG" alt="">
                        <div class="text">
                            <h3 class="second-title">SAM Medical醫療器材</h3>
                            <p>
                            SAM Medical 是位於美國奧勒岡州專精於開發創傷醫學急救器材公司本公司專為代理SAM Medical 台灣、中國及澳門之各項產品商業銷售業務。
                            </p>
                        </div>
                    </div>
                </div>
                <div class="inner">
                    <div class="container d-flex align-items-center flex-wrap">
                        <img src="./public/img/business-6.PNG" alt="">
                        <div class="text">
                            <h3 class="second-title">合作夥伴</h3>
                            <ol>
                                <li>Peli BioThermal</li>
                                <li>SAM Medical</li>
                                <li>XVIVO Perfusion</li>
                                <li>Nicolab</li>
                                <li>Taiwan Neuroimmunology Medical Society</li>
                                <li>EUROIMMUN (South East Asia) Pte. Ltd.</li>
                                <li>Quanterix</li>
                                <li>Waters Asia Ltd., Taiwan Branch (U.S.A)</li>
                                <li>UNIMAX Co., Ltd.</li>
                                <li>Cold Spring Biotech Corp.</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>