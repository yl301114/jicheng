<?php
    $pageTitle = ["產品介紹","INTRODUCTION"];
    $pagePic = '/public/img/page_title_product.png';
    $pageName = "introduction";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>

</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="list-pic-1">
            <div class="inner">
                <div class="container d-flex align-items-center flex-wrap">
                    <img src="./public/img/product-1.png" alt="">
                    <div class="text">
                        <h3 class="second-title">品牌簡介</h3>
                        <p>
                            美國Peli BioThermal™為全球知名的溫控物流包裝產品製造商。Peli BioThermal™致力於提供藥品、疫苗、血液製劑或生物性藥品、醫療器材及緊急救護用品等醫藥產品的妥善包裝。依照使用者的需求，提供其適合的包材尺寸、運送溫度與保冷時數的產品，可選擇買斷或租賃的方式來使用溫控包材。<br>
                            <br>
                            目前在新加坡、日本東京、韓國仁川、中國上海及印度孟買設有服務中心或租借站點，可以提供台灣客戶在產品進、出口時，溫控櫃的租賃與回收的服務。
                        </p>
                    </div>
                </div>
            </div>
            <div class="inner">
                <div class="container d-flex align-items-center flex-wrap">
                    <img src="./public/img/product-2.PNG" alt="">
                    <div class="text">
                        <h3 class="second-title">產品組成</h3>
                        <p>
                            Peli BioThermal™溫控箱採立方體設計，每一面均有溫控材料，讓箱體內部維持所需且均勻的溫度條件。可分為單次使用CoolGuard™系列與重複使用Crēdo Cube™系列。由外而內，分為三個組件：
                        </p>
                        <br>
                        <ol>
                            <li>外層：CoolGuard™為紙箱；Crēdo Cube™為塑料箱（特大箱型則為厚瓦楞箱），小箱型可另選背袋，方便攜行。</li>
                            <li>中層：CoolGuard™為高密度保麗龍（進階款含真空隔熱層VIP）；Crēdo Cube™整體使用絕緣性極佳的真空隔熱層VIP。</li>
                            <li>內層： CoolGuard™為冷磚BRICS；Crēdo Cube™為蓄冷板系統TIC™。內部填充的保冷劑為相變材料PCM，可使溫控箱內保持均勻的溫度。</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-pic-2 mt-55 mb-100">
            <h3 class="second-title text-center mb-20">產品規格</h3>
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="pic"><img src="./public/img/product-3.png" alt=""></div>
                        <h4>產品標題產品標題產品標題</h4>
                        <p>文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字</p>
                    </div>
                    <div class="col-md-4">
                        <div class="pic"><img src="./public/img/product-3.png" alt=""></div>
                        <h4>產品標題產品標題產品標題</h4>
                        <p>文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字</p>
                    </div>
                    <div class="col-md-4">
                        <div class="pic"><img src="./public/img/product-3.png" alt=""></div>
                        <h4>產品標題產品標題產品標題</h4>
                        <p>文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字描述文字</p>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>