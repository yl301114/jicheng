<?php
    $pageTitle = ["技術諮詢","CONSULT"];
    $pagePic = '/public/img/page_title_product.png';
    $pageName = "introduction";
    $contactClass = "gray-bg";
?>
<!DOCTYPE html>
<html lang="zh">
<head>
    <?php include("include/meta.php") ?>
</head>
<body>
    <?php include("include/header.php") ?>
    <main>
        <?php include("include/page-title.php") ?>
        <div class="container">
            <h3 class="second-title text-center mb-20">常見問答</h3>
            <div class="list-text">
                <div class="inner">
                    <div class="list-text_title">
                        <div class="tag-box"><div class="tag">Question</div></div>
                        <h3>Peli BioThermal™的保溫箱屬於主動式還是被動式的包材？</h3>
                        <div class="btn-collapse">
                            <div class="bar vertical"></div>
                            <div class="bar horizontal"></div>
                        </div>
                    </div>
                    <div class="list-text_content">
                        <p>
                            Peli BioThermal™全系列保溫箱（櫃）屬被動式冷鏈箱設計，均勻維持保溫箱冷度的來源是蓄冷片系統TIC™，不同於主動式保溫箱需要以電力（或其他能源）供給壓縮機系統進行降溫。只要將蓄冷片以原廠建議的預冷方式操作，可讓保溫箱在需要的條件溫度範圍內進行運輸。<br>
                            被動式保溫箱的優點在於，無須擔心陸路或航空運輸途中，保溫箱（櫃）是否還有充足的電力可持續維持箱體內溫度恆定。
                        </p>
                    </div>
                </div>
                <div class="inner">
                    <div class="list-text_title">
                        <div class="tag-box"><div class="tag">Question</div></div>
                        <h3>如何區分蓄冷片系統TIC™的使用溫層？</h3>
                        <div class="btn-collapse">
                            <div class="bar vertical"></div>
                            <div class="bar horizontal"></div>
                        </div>
                    </div>
                    <div class="list-text_content">
                        <p>
                            蓄冷片系統TIC™的使用溫層，以板片上標籤的顏色做區隔：<br>
                            Series 20M為-18℃，標籤顏色為灰色。<br>
                            Series 4為2~8℃，標籤顏色為藍色。<br>
                            Series 22為15~25℃，標籤顏色為棕色。<br>
                            （另有Series 50M為-65~-30℃，標籤顏色為紫紅色，若需此規格請來電另洽本公司業務人員）
                        </p>
                    </div>
                </div>
                <div class="inner">
                    <div class="list-text_title">
                        <div class="tag-box"><div class="tag">Question</div></div>
                        <h3>如何將蓄冷片系統TIC™進行預冷？</h3>
                        <div class="btn-collapse">
                            <div class="bar vertical"></div>
                            <div class="bar horizontal"></div>
                        </div>
                    </div>
                    <div class="list-text_content">
                        <p>
                            不同溫層的蓄冷片系統TIC™的預冷方式並不相同，以下為原廠建議的預冷方式：<br>
                            Series 20M  -18℃的預冷方式：。<br>
                            將TIC™板片個別平放，並在-65℃以下的溫度環境中，靜置至少24 小時（TIC™完成預冷所需的時間取決於板片數量和預冷的設備規格）。預冷處理後，板片裡的保冷劑PCM會完全呈現固態，搖晃板片確保聽不到液體流動聲，即完成預冷，可取用進行包裝。若無立即使用，可將板片可採六片一疊的方式堆放在-65℃以下環境中存放。<br>
                            若冷凍設備的溫度僅能達到-25℃，TIC™板片靜置預冷處理的時間就需大於24小時以上，為確保TIC™完全冷凍，需搖晃板片檢查確認是否聽不到液體聲。<br>
                            Series 4  2~8℃的預冷方式：<br>
                            將TIC™板片個別平放，並在-18℃以下的溫度環境中，靜置至少24小時（TIC™完成預冷所需的時間取決於板片數量和預冷的設備規格）。預冷處理後，板片裡的保冷劑PCM會完全呈現固態，搖晃板片確保聽不到液體流動聲，即完成預冷。<br>
                            若需立即使用，則須將板片進行回溫，可搭配紅外線溫測槍、TSI指示器或是原廠建議的回溫時間對照表，當回溫至2~5℃時就可以進行包裝；若無立即使用，可將板片採六片一疊的方式堆放在2~8℃冷藏環境中存放，需要使用時可立即取出進行包裝。<br>
                            Series 22  15~25℃的預冷方式：<br>
                            將TIC™板片個別平放，並在15~21℃的溫度環境中靜置至少24小時。預冷處理後，板片裡的保冷劑PCM會完全呈現固態，搖晃板片確保聽不到液體流動聲，即完成預冷，可取用進行包裝。若無立即使用，可將板片可採六片一疊的方式堆放在15~21℃環境中存放。
                        </p>
                    </div>
                </div>
                <div class="inner">
                    <div class="list-text_title">
                        <div class="tag-box"><div class="tag">Question</div></div>
                        <h3>如何清潔與維護Crēdo的零件？</h3>
                        <div class="btn-collapse">
                            <div class="bar vertical"></div>
                            <div class="bar horizontal"></div>
                        </div>
                    </div>
                    <div class="list-text_content">
                        <p>
                            塑料瓦楞外箱、真空隔熱層VIP的上蓋與底座內部、以及蓄冷板片，請使用微濕的抹布或毛巾擦拭，或是將消毒酒精噴灑於乾抹布或毛巾上進行擦拭，待表面水分蒸散後，再進行收納。<br>
                            不可將任何零件使用高壓蒸氣消毒、不可用有機溶劑或是含有磨砂顆粒的清潔用品進行擦拭。<br>
                            真空隔熱層VIP和蓄冷片系統TIC™不可放置於極端高溫（75℃以上）的環境下存放或測試。
                        </p>
                    </div>
                </div>
                <div class="inner">
                    <div class="list-text_title">
                        <div class="tag-box"><div class="tag">Question</div></div>
                        <h3>如何檢查和更換真空隔熱層VIP？</h3>
                        <div class="btn-collapse">
                            <div class="bar vertical"></div>
                            <div class="bar horizontal"></div>
                        </div>
                    </div>
                    <div class="list-text_content">
                        <p>
                            為保持Crēdo保溫箱中的真空隔熱層VIP內部真空狀態（鋁箔外膜皺縮），保持其良好的絕緣隔熱的狀態，應定期檢查VIP蓋板和底座表面。若表面的鋁箔外膜呈現光滑的情形，說明隔熱材料的真空效果已消失，應盡快更換。<br>
                            應避免從外層塑料瓦楞箱中拿出VIP的底座，降低其表面因外力而受損的情況。建議在VIP的表面所印刷的到期日前更換VIP零件。或是在VIP到期後，每年執行一次確效試驗以確保產品符合規格要求。
                        </p>
                    </div>
                </div>
                <div class="inner">
                    <div class="list-text_title">
                        <div class="tag-box"><div class="tag">Question</div></div>
                        <h3>Crēdo的保溫效果夏季與冬季是否有差異？</h3>
                        <div class="btn-collapse">
                            <div class="bar vertical"></div>
                            <div class="bar horizontal"></div>
                        </div>
                    </div>
                    <div class="list-text_content">
                        <p>
                            原廠規格所呈現的保溫時數，是在22~25℃環境溫度下所定義。<br>
                            依照原廠的夏季與冬季測試數據（模擬環境），夏季外在環境溫度為25~35℃時，規格所訂的保溫時數會縮減為原期望時數的7~8成；冬季外在環境溫度為-10~15℃時，規格所訂的保溫時數會比原期望時數再延長3~5成。<br>
                            無論外在環境溫度變化多大，Crēdo保溫箱只要妥善使用，就能夠長時間保持在條件溫度的狀態。
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("include/contact.php");?>
    </main>
    <?php include("include/footer.php") ?>
</body>
</html>